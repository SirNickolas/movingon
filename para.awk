#!/usr/bin/awk -E

{
    if (!$0)
        a = 0
    else if (a)
        print ""
    else
        a = 1
    print
}
