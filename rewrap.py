#!/usr/bin/env python
# coding: utf-8

from __future__ import print_function

import argparse
import io
import itertools
import textwrap


class Processor:
    def __init__(self):
        self._w = textwrap.TextWrapper(width=100, break_long_words=False, break_on_hyphens=False)

    def iter_paragraphs(self, lines):
        for _, para in itertools.groupby(map(str.rstrip, lines), key=bool):
            yield '\n'.join(self._w.wrap(' '.join(para)))

    def process(self, f):
        return '\n'.join(self.iter_paragraphs(f))


def main():
    p = Processor()
    parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
    parser.add_argument("files", metavar="<file>", nargs='*', default=['-'])
    for filename in parser.parse_args().files:
        if filename == '-':
            print(p.process(sys.stdin))
        else:
            with io.open(filename, encoding="utf-8-sig") as f:
                result = p.process(f)
            with io.open(filename, 'w', encoding="utf-8") as f:
                print(result, file=f)


if __name__ == "__main__":
    main()
