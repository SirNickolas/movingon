# Какой-то личный рувн-«Оскар»

Несколько отечественных визуальных новелл, вышедших (или показавших демо) за последний год и
которые, как кажется, есть за что отметить.

божок из «Рейда» как предлагаемая статуэтка

—————

Лучшая женская роль — monovalerie («Скрижали»), как единственная представленная и редкий случай
женской роли в вн вообще. Но она есть.

«Скрижали» — вн-эксперимент, сделавшая упор на видеоряд и соответствующую атмосферу. Всё действие
происходит в электричке… Или даже в одном её окне.

ничего необычного, просто актриса в визуальной новелле

—————

Лучшая героиня — Ника и Ева («Deadlock»)

Хотелось назвать Шарлотту, но зачем нам одна, если можно взять двух? А Шарлотте и так хватает
внимания (в том числе критики), ведь в её честь есть целая вн.

Ника и Ева — веселая и яркая, чем-то даже мемичная пара, задающая тон истории. Формально это не
главные герои, но без них было бы никуда, особенно при такой-то пассивности протагониста. «Дедлок» —
приятная ненавязчивая новелла, и во многом это заслуга милой парочки.

Ника и Ева

—————

Лучший герой — Руи — «Корона из листьев»

Неунывающий городской хлыщ, ювелир-недоучка и автор статей о научной магии…

Трудно обойти стороной «Корону из листьев» — она превосходна технически, но прежде всего выделяется
дизайном мира и персонажей.

В этом случае насыщенный мир напрямую дополняет героя: даже само его обиталище многое расскажет о
нём.

Сам Руи не так часто сможет удивлять своими поступками — всё-таки это персонаж вн с выборами, но его
рожки всегда будут с вами в левом нижнем углу.

эмоции Руи

—————

Лучший линейный сценарий — «Завеса»

«Завеса» — солидная постапокалиптическая драма, один из трех победителей Anivisual Contest #4

В вн с таким мрачным антуражем и тематикой на сценарий ложится двойная нагрузка. Здесь её получилось
выдержать. Повествование интригует, погружает в гроб, гроб-атмосферу… И уверенно, пусть и в
несколько холодной манере, стремится к логическому концу.

где-то там внутри бар

—————

Лучший нелинейный сценарий — «Рейд»

Нет сомнений по поводу этого пункта. Тот случай, когда ветвления действительно оправданы — не только
работают на детективную часть, но и содержат в себе самостоятельные и сильные части истории.

Сам «Рейд» — нетривиальная и отважно неоднозначная работа, которая может пугать, смешить, удивлять,
отвращать, шокировать, вызывать как восхищение, так и недоумение технической стороной и при этом
неведомым образом оставаться чем-то цельным.

кто же… а что мы вообще расследуем?

—————

Лучшая операторская работа — «Шарлотта»

Здесь очень и очень много анимации, включая использование известного скрипта 3D-камеры для Ren'Py.

«Шарлотта» — со всех сторон красивая история со своей тайной… и разгадкой, если вы до нее дойдете.
Своими силами получается не у всех.

Заслуженный призер Anivisual Contest #4

вот-вот заморгают, подождите еще минутку

—————

Лучшая работа художника-постановщика — «Так не бывает»

AnnDR поставила для своей вн шикарные художества. Чего же боле?

«Так не бывает» — отоме с изумительным антуражем приморского городка. Не такого мощного, как Находка
и Мурманск, но тоже чем-то манящего. Станет ли подобный вн-туризм трендом?

Интро: https://www.youtube.com/watch?v=2MeNrBuM1y8

—————

Лучшие костюмы — «Побег от принцессы»

В этой вн действительно много костюмов. Но ставка скорее на то, что героини своевременно от них
избавляются.

«Побег от принцессы» — очередная качественная работа от труженицы Salamandra88, заслуживающей также
звания и «лучшего режиссера»… Но мы не про персоналии.

Это юрийная новелла в красочном сеттинге сказки, щедрая на графику и хранящая даже незатейливый
твист.

—————

Лучший монтаж — «Коробка с темнотой»

Маленькая, но яркая звездочка с конкурса RUVN Contest #3. Завораживающая, стильная, держащая в
напряжении история в стиле фильмов Дэвида Линча.

Доказательство тому, что стоящая вн может родиться быстро и почти из ничего.

—————

Лучший саундтрек — «Эпитафия цветов»

Новелла попала в список призеров Anivisual Contest #4, в ней более 30-ти музыкальных треков, и как
раз музыка видится одним из ключей к относительному успеху.

В остальном произведение настолько возвышенное, что вряд ли простому смертному пристало писать о
нём.

—————

Лучший звук — «Город семи цветов»

Во-первых, там озвучен главный герой. Хотя бы частично. Во-вторых, озвучивает его главный эксперт
рувн по звукам — Jadevater, так что он сможет раскритиковать любой другой выбор, но не этот.

«Город семи цветов» — не слишком серьезный, отчасти пародийный детектив в сеттинге сказочного
города, где вместе уживаются люди, говорящие коты (либо люди, считающие себя котами и убедившие
прочих) и непременные кошкодевочки. Победитель RUVN Contest #3

—————

Лучшие визуальные эффекты — «Сателлит».

Если в «Шарлотте» анимацией потчуют постоянно по чайной ложке, то в «Сателлите» ей кормят реже, зато
сразу ведрами. Временами вн превращается в панорамный слоеный мультфильм, иллюстрирующий ту или иную
погоню.

«Сателлит» — масштабный постапокалипсис на колесах, с мини-играми и игривыми барышнями. А также с
роботами — малыми, средними и ГИГАНТСКИМИ.

—————

Лучшая вн на иностранном языке — Camp Buddy

Добрая ностальгическая вн о настоящем скаутском лагере, о тысяче мальчишеских занятий с утра до
ночи, соперничестве и победах. О настоящей дружбе в том возрасте, когда крепче дружбы ничего нет. По
сравнению с этим шедевром, манифестом скаутской романтики, БЛ выглядит лишь грустной фальшивкой про
ряженых взрослых женщин, куда-то девших всех пионеров и зачем-то занявших их место.

—————

А если шутки в сторону, то провозглашать что-либо лучшим не хочется. Список, как вы поняли, не про
лучшесть, а про приятное разнообразие, которым нас радуют авторы.

Пишите, пожалуйста, в комментариях, какие новеллы вам полюбились за последнее время.
