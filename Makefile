SRC := 3months lera nonexistent ruvn_oscar_2018
SRC := $(SRC:%=prose/%.md)

.PHONY: all $(SRC)

all: $(SRC)

$(SRC):
	./rewrap.py $@
